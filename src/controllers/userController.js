const { sucessCode, errorCode, failCode } = require("../config/reponse");
const sequelize = require("../models/index");
const init_models = require("../models/init-models");
const model = init_models(sequelize);

//Post: Create Like_res
const createLike = async (req, res) => {
  try {
    let { user_id, res_id, date_like } = req.body;

    let result = await model.like_res.create({
      user_id,
      res_id,
      date_like,
    });
    sucessCode(res, result, "Người dùng like thành công!!!");
  } catch (err) {
    errorCode(res, "Lỗi Backend");
  }
};

// Delete: UnLike
const UnLike = async (req, res) => {
  try {
    let { user_id, res_id } = req.params;
    let checkUser = await model.like_res.findOne({
      where: {
        user_id,
        res_id,
      },
    });

    if (checkUser) {
      model.like_res.destroy({
        where: {
          user_id,
          res_id,
        },
      });
      sucessCode(res, checkUser, "Xoá like thành công!!!");
    } else {
      failCode(
        res,
        user_id,
        `User thứ ${user_id} chưa like cho nhà hàng thứ ${res_id} này!!!`
      );
    }
  } catch (err) {
    errorCode(res, "Lỗi Backend");
  }
};

//Post: Create Rate_res
const createRateRes = async (req, res) => {
  try {
    let { user_id, res_id, amount, date_rate } = req.body;

    let result = await model.rate_res.create({
      user_id,
      res_id,
      amount,
      date_rate,
    });
    sucessCode(res, result, "Người dùng đánh giá thành công!!!");
  } catch (err) {
    errorCode(res, "Lỗi Backend");
  }
};

//Post: Create Order
const createOrder = async (req, res) => {
  try {
    let { user_id, food_id, amount, code, arr_sub_id } = req.body;

    let result = await model.order.create({
      user_id,
      food_id,
      amount,
      code,
      arr_sub_id,
    });
    sucessCode(res, result, "Người dùng order thành công!!!");
  } catch (err) {
    errorCode(res, "Lỗi Backend");
  }
};

module.exports = { createLike, UnLike, createRateRes, createOrder };
