const express = require("express");
const restaurantRoute = require("./restaurantRoutes");
const rootRoute = express.Router();
const userRoute = require("./userRoutes");

rootRoute.use("/user", userRoute);

rootRoute.use("/restaurant", restaurantRoute);

module.exports = rootRoute;
