const express = require("express");
const {
  getListLike,
  getListRateRes,
  getListLikeResId,
  getListLikeUserId,
  getListRateResId,
  getListRateUserId,
} = require("../controllers/restaurantController");
const restaurantRoute = express.Router();

restaurantRoute.get("/getListLike", getListLike);

restaurantRoute.get("/getListLikeRes/:res_id", getListLikeResId);

restaurantRoute.get("/getListLikeUser/:user_id", getListLikeUserId);

restaurantRoute.get("/getListRateRes", getListRateRes);

restaurantRoute.get("/getListRateRes/:res_id", getListRateResId);

restaurantRoute.get("/getListRateUser/:user_id", getListRateUserId);

module.exports = restaurantRoute;
