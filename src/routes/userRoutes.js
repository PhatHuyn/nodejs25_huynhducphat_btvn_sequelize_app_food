const express = require("express");
const userRoute = express.Router();
const {
  createLike,
  UnLike,
  createRateRes,
  createOrder,
} = require("../controllers/userController");

userRoute.post("/createLike", createLike);

userRoute.post("/createRateRes", createRateRes);

userRoute.post("/createOrder", createOrder);

userRoute.delete("/UnLike/:user_id/:res_id", UnLike);

module.exports = userRoute;
